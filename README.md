# SEB-Server & SEB Screen Proctoring Deployment Dev

This deployment configuration is pulled by ArgoCD automatically. 
Any manual changes done directly with kubectl or in the Ranger GUI will eventually be overwritten.

## Development
The deployment gets its Kubernetes deployment base from `./seb-server-setup-dev`. This git submodule
tracks the *development* branch and uses the latest commits.

## Testing
Testing gets its Kubernetes deployment from `./seb-server-setup-test`. This submodule **must** be
pinned to a git tag. Progression to instance *staging* will use this tag!

To update the deployment:

1. Change working dir: `cd ./seb-server-testing`
1. Checkout the desired tag, ie.: `git checkout v2.1-latest`
1. Change back to main repo: `cd ..`
1. Add and commit: 
   ```
   git add seb-server-setup-test
   git commit -m "Updated configuration base to v2.1-latest"
   git push
   ```

A minute later the new configuration will be applied.

Be aware that changes in the base template may require adaptions to the instance configuration.
